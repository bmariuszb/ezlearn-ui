use stylist::{global_style, yew::use_style};
use yew::prelude::*;
use yew_router::prelude::*;

#[derive(Clone, Routable, PartialEq)]
enum Route {
    #[at("/")]
    Home,
    #[at("/blogs")]
    Blogs,
    #[at("/showcase")]
    Showcase,
    #[at("/technical-skills")]
    TechnicalSkills,
    #[at("/why")]
    Why,
    #[not_found]
    #[at("/404")]
    NotFound,
}

#[function_component(Home)]
fn home() -> Html {
    let style = use_style!(
        r#"
        margin-left: 12px;
        "#
    );
    html! {
        <>
            <Sidebar/>
            <div class={style}>
                <h1>{"Streamlining IT and beyond"}</h1>
                <p>{"Read blog posts, explore my projects and learn about my technical skills."}</p>
            </div>
        </>
    }
}

#[function_component(Blogs)]
fn blogs() -> Html {
    let style = use_style!(
        r#"
        margin-left: 12px;
        "#
    );
    html! {
        <>
            <Sidebar/>
            <div class={style}>
                <h1>{"Blogs"}</h1>
                <p>{"Coming soon! Here is the preview of what I'm working on:"}</p>
                <ul>
                    <li>{"What is http?"}</li>
                    <li>{"What is TCP, UDP?"}</li>
                    <li>{"What is a socket?"}</li>
                    <li>{"What is a WebSocket"}</li>
                    <li>{"UDP, TCP hole punching"}</li>
                    <li>{"What is factory?"}</li>
                    <li>{"What is dependency injection?"}</li>
                </ul>
            </div>
        </>
    }
}

#[function_component(Showcase)]
fn showcase() -> Html {
    let style = use_style!(
        r#"
        li::marker {
            font-size: 1.5rem;
        }
        padding-left: 12px;
        h2, .desc {
            display: inline;
        }
    "#
    );
    html! {
        <>
            <Sidebar/>
            <div class={style}>
                <h1>{"Showcase of my projects"}</h1>
                <ol>
                    <li>
                        <h2>{"Ezlearn"}</h2>
                        <p class={"desc"}>{" - website to host my blog post, showcase projects, technical skills and more. Self hosted on a free Oracle VM, written in Rust using Actix Web and Yew."}</p>
                        <p>{"Available "}<a href={"/"}>{"online"}</a>{" - you're using it right now"}</p>
                        <p>
                            {"Source code: "}
                            <a href={"https://gitlab.com/bmariuszb/ezlearn"}>{"backend"}</a>
                            {" "}
                            <a href={"https://gitlab.com/bmariuszb/ezlearn-ui"}>{"frontend"}</a>
                        </p>
                    </li>
                    <li>
                        <h2>{"Rubik's cube"}</h2>
                        <p class={"desc"}>{" - simulation of a standard 3x3 Rubik's cube in a browser written in Rust and compiled to WebAssembly."}</p>
                        <p>{"Available "}<a href={"/showcase/rubiks-cube"}>{"online"}</a>{" - WebGL2 required"}</p>
                        <a href={"https://gitlab.com/bmariuszb/rubiks-cube-web"}>{"Source code"}</a>
                    </li>
                </ol>
            </div>
        </>
    }
}

#[function_component(TechnicalSkills)]
fn technical_skills() -> Html {
    let style = use_style!(
        r#"
        padding-left: 12px;
    "#
    );
    html! {
        <>
            <Sidebar/>
            <div class={style}>
                <h1>{"Overview of technical skills used in projects"}</h1>
                <p>{"Click each for details."}</p>
                <p>{"Programming languages:"}</p>
                <ul>
                    <li>{"Rust"}</li>
                    <li>{"JavaScript"}</li>
                    <li>{"Python"}</li>
                </ul>
                <p>{"Technologies:"}</p>
                <ul>
                    <li>{"Docker"}</li>
                    <li>{"GitHub Actions"}</li>
                    <li>{"GitLab CI/CD"}</li>
                    <li>{"Ansible"}</li>
                    <li>{"Nginx"}</li>
                    <li>{"Certbot"}</li>
                </ul>
                <p>{"Algorithms and data structures:"}</p>
            </div>
        </>
    }
}

#[function_component(Why)]
fn why() -> Html {
    let style = use_style!(
        r#"
        padding-left: 12px;
    "#
    );
    html! {
        <>
            <Sidebar/>
            <div class={style}>
                <h1>{"Why I created this website?"}</h1>
                <ul>
                    <li>{"Create a knowledge base"}</li>
                    <li>{"Help others discover and understand new things"}</li>
                    <li>{"Learn Yew and Actix Web"}</li>
                    <li>{"Motivate myself to learn more about IT"}</li>
                    <li>{"Present my IT skills to potential recruiters"}</li>
                </ul>
            </div>
        </>
    }
}

#[derive(Clone, PartialEq, Properties, Default)]
struct State {
    sidebar_hidden: bool,
}

#[function_component(Sidebar)]
fn sidebar() -> Html {
    let state = use_state(State::default);
    let style = use_style!(
        r#"
        display: flex;
        min-width: 11rem;
        div {
            padding-top: 0.5rem;
        }
        a {
            display: inline-block;
            padding: 0 1rem 0 1rem;
        }
    "#
    );
    let hide_sidebar = {
        let state = state.clone();
        move |_| {
            state.set(State {
                sidebar_hidden: true,
            });
        }
    };
    let show_sidebar = {
        let state = state.clone();
        move |_| {
            state.set(State {
                sidebar_hidden: false,
            });
        }
    };

    if !state.sidebar_hidden {
        html! {
            <div class={style}>
                <div>
                    <Link<Route> to={Route::Home}>{"Home"}</Link<Route>><br />
                    <Link<Route> to={Route::Blogs}>{"Blogs"}</Link<Route>><br />
                    <Link<Route> to={Route::Showcase}>{"Showcase"}</Link<Route>><br />
                    <Link<Route> to={Route::TechnicalSkills}>{"Technical skills"}</Link<Route>><br />
                    <Link<Route> to={Route::Why}>{"Why?"}</Link<Route>>
                </div>
                <button onclick={hide_sidebar}>{"<"}</button>
            </div>
        }
    } else {
        html! {
            <div class={style}>
                <button onclick={show_sidebar}>{">"}</button>
            </div>
        }
    }
}

fn not_found_page() -> Html {
    html! {
        <div style="text-align: center; margin-top: 50px; min-width: 100vw">
            <h1 style="font-size: 3em; color: #555;">{"404 - Not Found"}</h1>
            <p>{"Oops! The page you are looking for could not be found."}</p>
            <p>{"Please check the URL or navigate back to the "}
                <Link<Route> to={Route::Home}>{"homepage."}</Link<Route>>
            </p>
        </div>
    }
}

fn switch(routes: Route) -> Html {
    match routes {
        Route::Home => html! { <Home/> },
        Route::Blogs => html! { <Blogs/> },
        Route::Showcase => html! { <Showcase/> },
        Route::TechnicalSkills => html! { <TechnicalSkills/> },
        Route::Why => html! { <Why/> },
        Route::NotFound => not_found_page(),
    }
}

#[function_component(Main)]
fn app() -> Html {
    let _html_style = global_style!(
        r#"
        body {
            margin: 0;
            min-height: 100vh;
            display: flex;
            background-color: #161616;
            color: #e0e0e0;
        }
        a:link {
            color: #e0e0e0;
        }
        a:visited {
            color: #eb33a7;
        }
        a:hover, button {
            color: #aaeb13;
        }
        button {
            background-color: #232323;
            border: 0;
            padding: 8px;
            font-size: 2rem;
        }
        button:hover {
            color: #0f0f0f;
            background-color: #334606;
        }
    "#
    );
    html! {
        <BrowserRouter>
            <Switch<Route> render={switch} />
        </BrowserRouter>
    }
}

fn main() {
    yew::Renderer::<Main>::new().render();
}
