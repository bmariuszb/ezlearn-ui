# Ezlearn UI
Frontend for [ezlearn](https://ezlearn.ix.tc) website


# Dependencies
Development: 
```console
cargo install trunk
```

# Run
```ln deploy/index.html index.html``` - create hard link for trunk
```trunk serve --open``` - tracks file changes, automatically recompiles project and reloads webpage
